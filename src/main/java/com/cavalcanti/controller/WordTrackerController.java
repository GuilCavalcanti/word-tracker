package com.cavalcanti.controller;

import com.cavalcanti.entity.Word;
import com.cavalcanti.repository.WordRepository;
import com.cavalcanti.service.WordService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/v1/words")
public class WordTrackerController {
    @Autowired
    private WordService wordService;

    @GetMapping
    public List<Word> getWords() {
        return wordService.getWords();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Word getWord(@PathVariable Long id) {
        return wordService.getWord(id);
    }

    @PostMapping
    public Word setWord(@RequestBody Word word) {
        return wordService.setWord(word);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PATCH)
    public Word updateWord(@PathVariable Long id, @RequestBody Word word) {
        return wordService.updateWord(id, word);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteWord(@PathVariable Long id) {
        wordService.deleteWord(id);
    }
}
