package com.cavalcanti.service;

import com.cavalcanti.entity.Word;
import org.springframework.stereotype.Service;

import java.util.List;

public interface WordService {

    List<Word> getWords();
    Word getWord(Long id);
    Word setWord(Word word);
    Word updateWord(Long id, Word word);
    void deleteWord(Long id);
}
