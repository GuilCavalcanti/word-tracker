package com.cavalcanti.service;

import com.cavalcanti.entity.Word;
import com.cavalcanti.repository.WordRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WordServiceImpl implements WordService{
    @Autowired
    private WordRepository wordRepository;

    @Override
    public List<Word> getWords() {
        return wordRepository.findAll();
    }

    @Override
    public Word getWord(Long id) {

        //Checking if the word with the given Id is present on the repository
        if (wordRepository.findById(id).isPresent()) {
            //Returning the word with the given Id
            return wordRepository.findById(id).get();
        } else {
            return null;
        }
    }

    @Override
    public Word setWord(Word word) { return wordRepository.saveAndFlush(word); }

    @Override
    public Word updateWord(Long id, Word word) {

        //Checking if the word with the given Id is present on the repository
        if (wordRepository.findById(id).isPresent()) {
            Word existingWord = wordRepository.findById(id).get();
            //Updating the word with the given Id with the properties of the word received
            BeanUtils.copyProperties(word, existingWord, "id");
            return wordRepository.save(existingWord);
        } else {
            return null;
        }
    }

    @Override
    public void deleteWord(Long id) {

        //Checking if the word with the given Id is present on the repository
        if (wordRepository.findById(id).isPresent()) {
            wordRepository.deleteById(id);
        }
    }
}
