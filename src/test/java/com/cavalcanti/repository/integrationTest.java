package com.cavalcanti.repository;

import com.cavalcanti.entity.Word;
import com.cavalcanti.service.WordService;
import org.assertj.core.api.Assertions;
import org.assertj.core.data.Index;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class integrationTest {

    @Autowired
    private WordService wordService;

    @Test
    public void testCreateReadDelete() {
        Word wordOne = new Word("Awe", "English");
        Word wordTwo = new Word("Awesome", "English");
        Word newWordOne = new Word("Incrível", "Portuguese");

        wordService.setWord(wordOne);
        wordService.setWord(wordTwo);

        List<Word> words = wordService.getWords();
        //Testing if getWords returns the right a List<Words> of the right size
        assertEquals(words.size(), 2);

        //Checking the contents of the Words in repository
        Assertions.assertThat(words).extracting(Word::getWord).contains("Awe", Index.atIndex(0));
        Assertions.assertThat(words).extracting(Word::getWord).contains("Awesome", Index.atIndex(1));

        wordService.updateWord(1L, newWordOne);

        words = wordService.getWords();
        //Checking if the size of words wasn't alter after updateWord() was called
        assertEquals(words.size(), 2);

        //Checking if Word with Id of 1 was updated
        Assertions.assertThat(words).extracting(Word::getWord).contains("Incrível", Index.atIndex(0));

        wordService.deleteWord(1L);
        words = wordService.getWords();

        //Checking if Word with Id of 1 was deleted
        Assertions.assertThat(words).extracting(Word::getId).contains(2L, Index.atIndex(0));
        assertEquals(words.size(), 1);
    }
}
