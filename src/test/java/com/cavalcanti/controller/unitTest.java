package com.cavalcanti.controller;


import com.cavalcanti.entity.Word;
import com.cavalcanti.repository.WordRepository;
import com.cavalcanti.service.WordService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(WordTrackerController.class)
public class unitTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WordService wordService;

    @MockBean
    private WordRepository wordRepository;

    @Test
    public void testGetWords() throws Exception {

        Word word = new Word("Exit", "English");

        mockMvc.perform(get("/api/v1/words"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[]")).andReturn();

        verify(wordService, times(1)).getWords();
    }

    @Test
    public void testGetWord() throws Exception {

        Word word = new Word("Exit", "English");
        word.setId(1L);

        when(wordService.getWord(word.getId())).thenReturn(word);

        mockMvc.perform(get("/api/v1/words/" + word.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.word", Matchers.is("Exit")));

        verify(wordService, times(1)).getWord(word.getId());
    }

    @Test
    public void testSetWord() throws Exception {

        Word word = new Word("Enter", "English");

        //Parsing word into JSON
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(word);

        Mockito.when(wordService.setWord(Mockito.any(Word.class))).thenReturn(word);

        mockMvc.perform(post("/api/v1/words")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.word", Matchers.is("Enter")))
                .andReturn();

        verify(wordService, times(1)).setWord(Mockito.refEq(word));
    }

    @Test
    public void testUpdateWord() throws Exception {

        Word word = new Word("Enter", "English");
        word.setId(1L);

        //Parsing word into JSON
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(word);

        Mockito.when(wordService.updateWord(eq(word.getId()), Mockito.refEq(word))).thenReturn(word);

        mockMvc.perform(patch("/api/v1/words/" + word.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.word", Matchers.is("Enter")));

        verify(wordService, times(1)).updateWord(Mockito.eq(word.getId()), Mockito.refEq(word));
    }

    @Test
    public void testDelete() throws Exception {

        long wordIdToBeDeleted = 1L;

        MvcResult result = mockMvc.perform(delete("/api/v1/words/" + wordIdToBeDeleted))
                .andExpect(status().isOk()).andReturn();

        verify(wordService, times(1)).deleteWord(wordIdToBeDeleted);
    }
}
